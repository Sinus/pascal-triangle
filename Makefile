CC=gcc
CFLAGS=-ansi -Wall --pedantic
SRC=pascal.c err.c err.h

pas: $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o pascal

.PHONY: clean
clean:
	rm -rf pascal
