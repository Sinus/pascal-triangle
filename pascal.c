/*
 * Marcin Dominiak : md346906
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "err.h"

void W(int nr, int n, int leftW, int leftR, int rightW, int rightR)
{
    int val = 1;
    int oldVal = 1;
    int leftOld = 0;
    int command;
    int howLong;
    int rVal;
    int i;
    int done = 1;

    if (nr == n) /*close unnecesery pipes in last process*/
    {
        if (close(rightR) == -1)
            syserr("Error in close\n");
        if (close(rightW) == -1)
            syserr("Error in close\n");
        if (write(leftW, &done, sizeof(done)) == -1) /*signal that last W ready*/
            syserr("Error in write\n");
    }
    else /*wait untill every worker is created and ready to go*/
    {
        if(read(rightR, &done, sizeof(done)) == -1)
            syserr("Error in read\n");
        if (nr != 1)
        {
            if (write(leftW, &done, sizeof(done)) == -1)/*signal to go*/
                syserr("Error in write\n");
        }
    }

    while (1) /*wait for orders from pascal*/
    {
        if (read(leftR, &command, sizeof(command)) == -1)
            syserr("Error in read\n");
        switch (command)
        {
            case -1: /*calculate*/
                if (read(leftR, &howLong, sizeof(howLong)) == -1) /*level of triangle*/
                    syserr("Error in read\n");

                if (nr == howLong)
                    val = 1;

                if (nr != 1) /*get old value from neighbour to calculate our's*/
                    if (read(leftR, &leftOld, sizeof(leftOld)) == -1)
                        syserr("Error in read\n");

                if ((nr != 1) && (nr != howLong))
                    val = oldVal + leftOld;

                if (nr != howLong) /*send command forward and pass od value*/
                {
                    if (write(rightW, &command, sizeof(command)) == -1)
                        syserr("Error in write\n");
                    if (write(rightW, &howLong, sizeof(howLong)) == -1)
                        syserr("Error in write\n");
                    if (write(rightW, &oldVal, sizeof(oldVal)) == -1)
                        syserr("Error in write\n");
                }

                if (nr != howLong) /*wait for everyone to finish*/
                    if (read(rightR, &done, sizeof(done)) == -1)
                        syserr("Error in read\n");

                if (write(leftW, &done, sizeof(done)) == -1) /*end*/
                    syserr("Error in write\n");
                oldVal = val;
                break;

            case -2: /*get results*/              
                if (write(leftW, &val, sizeof(val)) == -1) /*pass our result*/
                    syserr("Error in write\n");

                if (write(rightW, &command, sizeof(command)) == -1)
                    syserr("Error in write\n");

                for (i = nr; i <= n; i++) /*read and pass results of the rest*/
                {
                    if (read(rightR, &rVal, sizeof(rVal)) == -1)
                        syserr("Error in read\n");
                    if (write(leftW, &rVal, sizeof(rVal)) == -1)
                        syserr("Error in write\n");
                }
                break;

            case -3: /*die*/
                if (nr == n)
                    exit(0);

                else /*wait for children to die and then die*/
                {
                    if (write(rightW, &command, sizeof(command)) == -1)
                        syserr("Error in write\n");
                    if (wait(0) == -1)
                        syserr("Error in wait\n");
                    exit(0);
                }
                break;
        }
    }
}

int main(int argc, char* argv[])
{
    pid_t pid;
    int pipeDsc[2][2];
    int pipeOld[2];
    int leftPipe[2];
    int n = atoi(argv[1]) + 1; /*+1 for pascal*/
    int pascal = 0;
    int temp;
    int command; /*-1 - calculate; -2 - get result; -3 - die*/
    int i;

    for (i = 0; i < n; i++) /*create a list of processes*/
    {

        pipeOld[0] = pipeDsc[0][0];
        pipeOld[1] = pipeDsc[1][1];
        leftPipe[0] = pipeDsc[0][0];
        leftPipe[1] = pipeDsc[1][1];

        if (pipe(pipeDsc[0]) == -1) /*pipes for reading and writing*/
            syserr("Error in pipe\n");
        if (pipe(pipeDsc[1]) == -1)
            syserr("Error in pipe\n");
        switch(pid = fork())
        {
            case -1:
                syserr("Error in fork\n");

            case 0: /*child*/
                if (close(pipeDsc[0][1]) == -1)
                    syserr("Error in close\n");
                if (close(pipeDsc[1][0]) == -1)
                    syserr("Error in close\n");

                if (i > 0) /*not 1st child*/
                {
                    if (close(pipeOld[0]) == -1)
                        syserr("Error in close\n");
                    if (close(pipeOld[1]) == -1)
                        syserr("Error in close\n");
                }

                if (i == n - 1) /*last child will not become worker*/
                    exit(0);
                break; 

            default: /*parent*/
                if (close(pipeDsc[0][0]) == -1)
                    syserr("Error in close\n");
                if (close(pipeDsc[1][1]) == -1)
                    syserr("Error in close\n");
                if (i == 0)
                    pascal = 1;
                else
                    W(i, n, leftPipe[1], leftPipe[0],
                            pipeDsc[0][1], pipeDsc[1][0]); /*become worker*/
        }
        if (pascal == 1)
            break; /*go to pascal section*/
    }

    /*only pascal comes here*/
    command = -1; /*calculate results*/
    for (i = 1; i < n; i++)
    {
        if (write(pipeDsc[0][1], &command, sizeof(command)) == -1)
            syserr("Error in write\n");

        if (write(pipeDsc[0][1], &i, sizeof(i)) == -1) /*which step is it*/
            syserr("Error in write\n");

        if (read(pipeDsc[1][0], &temp, sizeof(temp)) == -1) /*step done*/
            syserr("Error in read\n");
    }

    command = -2; /*collect results to an array*/
    
    if (write(pipeDsc[0][1], &command, sizeof(command)) == -1)
        syserr("Error in write\n");

    for (i = 1; i < n; i++)
    {
        if (read(pipeDsc[1][0], &temp, sizeof(temp)) == -1) /*i-th value*/
            syserr("Error in read\n");
        printf("%d ", temp);
    }
    printf("\n");

    command = -3; /*kill workers*/
    if (write(pipeDsc[0][1], &command, sizeof(command)) == -1)
        syserr("Error in write\n");
    if (wait(0) == -1)
        syserr("Error in wait\n");
    return 0;
}
